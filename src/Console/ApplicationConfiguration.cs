﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;

namespace Michaelkc.FileEventer
{
    internal class ApplicationConfiguration
    {
        public string PathToMonitor { get; set; }

        public string Filetype { get; set; }
        public TimeSpan ReactionDelay { get; set; }

        public static ApplicationConfiguration CreateFromConfig()
        {
            return new ApplicationConfiguration()
            {
                PathToMonitor = @"c:\junk\test\",
                Filetype = "*.jpg",
                ReactionDelay = TimeSpan.FromSeconds(5)
            };
        }
    }
}
