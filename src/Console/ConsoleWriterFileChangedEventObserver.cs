﻿using System;
using System.IO;
using Serilog;

namespace Michaelkc.FileEventer
{
    internal class ConsoleWriterFileChangedEventObserver : IObserver<FileChangedEvent>
    {
        private readonly ILogger _log = Log.Logger.ForContext<ConsoleWriterFileChangedEventObserver>();

        public void OnNext(FileChangedEvent value)
        {
            _log.Information(string.Format("ConsoleWriterFileChangedEventObserver.OnNext{0}", value));
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }
    }
}