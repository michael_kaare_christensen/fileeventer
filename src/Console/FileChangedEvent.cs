﻿using System.IO;
using System.Reactive;

namespace Michaelkc.FileEventer
{
    public class FileChangedEvent
    {
        public EventPattern<RenamedEventArgs> RenamedEventData { get; set; }
        public EventPattern<FileSystemEventArgs> ChangedEventData { get; set; }

        public string FullPath { get; set; }
        public FileChangedEvent(EventPattern<FileSystemEventArgs> changedEventData)
        {
            ChangedEventData = changedEventData;
            FullPath = changedEventData.EventArgs.FullPath;
        }

        public FileChangedEvent(EventPattern<RenamedEventArgs> renamedEventData)
        {
            RenamedEventData = renamedEventData;
            FullPath = renamedEventData.EventArgs.FullPath;
        }
    }
}