using System;
using Serilog;
using Serilog.Events;
using Serilog.Formatting.Json;
using Serilog.Sinks.RollingFile;

namespace Michaelkc.FileEventer.Infrastructure
{
    internal class SerilogConfig
    {
        public static ILogger CreateLogger()
        {
            var config = new LoggerConfiguration();
            Log.Logger = config.
                WriteTo.ColoredConsole()
                                .CreateLogger();
                //WriteTo.Sink(new RollingFileSink("log-debug-{Date}.json", new JsonFormatter(renderMessage: true), null, null), LogEventLevel.Debug).
                //WriteTo.RollingFile("log-debug-{Date}.log", LogEventLevel.Debug).
                //WriteTo.Sink(new RollingFileSink("log-{Date}.json", new JsonFormatter(renderMessage: true), null, null), LogEventLevel.Warning);
            //InitialiseGlobalContext(config);
            Log.Logger.Information("SeriLog configured");
            return Log.Logger;
        }

        public static LoggerConfiguration InitialiseGlobalContext(LoggerConfiguration configuration)
        {
            return configuration.Enrich.WithMachineName()
                .Enrich.WithProperty("ApplicationName", typeof(SerilogConfig).Assembly.GetName().Name)
                .Enrich.WithProperty("UserName", Environment.UserName)
                .Enrich.WithProperty("AppDomain", AppDomain.CurrentDomain)
                .Enrich.WithProperty("RuntimeVersion", Environment.Version)
                // this ensures that calls to LogContext.PushProperty will cause the logger to be enriched
                .Enrich.FromLogContext();
        }
    }
}