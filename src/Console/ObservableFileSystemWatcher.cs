﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using Serilog;

namespace Michaelkc.FileEventer
{
    internal class ObservableFileSystemWatcher
    {
        private readonly ApplicationConfiguration _configuration;
        private readonly IObserver<FileChangedEvent>[] _fileActions;
        private readonly ILogger _log = Log.Logger.ForContext<ObservableFileSystemWatcher>();
        private IObservable<FileChangedEvent> _watcher;
        private IEnumerable<IDisposable> _subscriptions = new IDisposable[0];

        private IObservable<FileChangedEvent> ObserveFolderChanges(string path, string filter, TimeSpan throttle)
        {
            var fileSystemWatcher = new FileSystemWatcher(path, filter)
            {
                EnableRaisingEvents = true,
                IncludeSubdirectories = true
            };

            var sources = new[] 
                { 
                    Observable.FromEventPattern<FileSystemEventArgs>(fileSystemWatcher, "Created")
                              .Select(ev => new FileChangedEvent(ev)),
 
                    Observable.FromEventPattern<FileSystemEventArgs>(fileSystemWatcher, "Changed")
                              .Select(ev => new FileChangedEvent(ev)),
 
                    Observable.FromEventPattern<RenamedEventArgs>(fileSystemWatcher, "Renamed")
                              .Select(ev => new FileChangedEvent(ev)),
 
                    Observable.FromEventPattern<FileSystemEventArgs>(fileSystemWatcher, "Deleted")
                              .Select(ev => new FileChangedEvent(ev)),
 
                    Observable.FromEventPattern<ErrorEventArgs>(fileSystemWatcher, "Error")
                              .SelectMany(ev => Observable.Throw<FileChangedEvent>(ev.EventArgs.GetException()))
                };

            return sources.Merge()
                .DistinctUntilChanged(f => f.FullPath)
                .Delay(throttle)
                .Finally(() => fileSystemWatcher.Dispose());
        }
        public ObservableFileSystemWatcher(ApplicationConfiguration configuration, IObserver<FileChangedEvent>[] fileActions, ILogger log)
        {
            _fileActions = fileActions;
            _configuration = configuration;

        }

        public void Start()
        {
            _watcher =
            ObserveFolderChanges(
                _configuration.PathToMonitor,
                _configuration.Filetype,
                _configuration.ReactionDelay
                );

            _subscriptions = _fileActions.Select(f =>
            {
                _log.Debug("Subscribing " + f.GetType());
                return _watcher.Subscribe(f);

            }).ToArray();
        }

        public void Stop()
        {
            foreach (var s in _subscriptions)            
            {
                _log.Debug("Unsubscribing subscriber");
                s.Dispose();
            }
            _watcher = null;
        }
    }
}