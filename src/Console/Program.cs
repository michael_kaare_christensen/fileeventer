﻿using System;
using System.IO;
using System.Reflection;
using Autofac;
using AutofacSerilogIntegration;
using Michaelkc.FileEventer.Infrastructure;
using Serilog;
using Topshelf;
using Topshelf.Autofac;

namespace Michaelkc.FileEventer
{
    internal class Program
    {
        private static readonly ILogger _log = Log.Logger.ForContext<Program>();

        public static void Main()
        {
            ConfigureLogging();
            LoadPlugins();
            var container = CreateAutofacContainer();
            PrintPlugins(container);
            var host = CreateTopshelfHost(container);
            host.Run();
        }

        private static void LoadPlugins()
        {
            var pluginDir = new DirectoryInfo(Path.Combine(Environment.CurrentDirectory, "plugins"));
            var pluginAssemblies = pluginDir.GetFiles("*.dll");
            foreach (var pluginAssembly in pluginAssemblies)
            {
                _log.Information("Loading {0}", pluginAssembly);
                Assembly.LoadFile(pluginAssembly.FullName);
            }
        }

        private static void PrintPlugins(IContainer container)
        {
            var plugins = container.Resolve<IObserver<FileChangedEvent>[]>();
            _log.Information("Plugins:");
            foreach (var plugin in plugins)
            {
                _log.Information("\t" + plugin.GetType().FullName);
            }
        }

        private static Host CreateTopshelfHost(IContainer container)
        {
            return HostFactory
                .New(x =>
                {
                    x.UseAutofacContainer(container);
                    x.UseSerilog(Log.Logger);
                    x.Service<ObservableFileSystemWatcher>(s =>                        //2
                    {
                        s.ConstructUsingAutofacContainer();     //3
                        s.WhenStarted(tc => tc.Start());              //4
                        s.WhenStopped(tc => tc.Stop());               //5
                    });
                    x.RunAsLocalSystem();                            //6

                    x.SetDescription("Sample Topshelf Host");        //7
                    x.SetDisplayName("Stuff");                       //8
                    x.SetServiceName("Stuff");                       //9
                });
        }

        private static IContainer CreateAutofacContainer()
        {
            var builder = new ContainerBuilder();

            // Does not seem to work, ILogger resolves to silentlogger in consumers?
            builder.RegisterLogger(Log.Logger,true);
            
            builder
                .RegisterAssemblyTypes(AppDomain.CurrentDomain.GetAssemblies())
                .Except<ApplicationConfiguration>()
                //.Except<ConsoleWriterFileChangedEventObserver>()
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            builder
                .RegisterInstance(ApplicationConfiguration.CreateFromConfig())
                .AsSelf()
                .SingleInstance();
            var container = builder.Build();
            return container;
        }

        private static void ConfigureLogging()
        {
            Log.Logger = SerilogConfig.CreateLogger();
        }
    }
}
