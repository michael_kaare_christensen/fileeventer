using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Json;
using Serilog;

namespace FileEventer.FlickrRenameAndCopy
{
    public class FlickrRenameAndCopyConfiguration
    {
        private readonly Dictionary<string, string> _configuration;
        private readonly ILogger _log = Log.Logger.ForContext<FlickrRenameAndCopyConfiguration>();

        public FlickrRenameAndCopyConfiguration()
        {
            _log.Debug("Reading config");
            var config = Assembly.GetExecutingAssembly().Location.Replace(".dll",".json");
            var serializer = new DataContractJsonSerializer(typeof (Dictionary<string, string>));

            using (var fs = new FileStream(config,FileMode.Open))
            {
                _configuration = (Dictionary<string, string>) serializer.ReadObject(fs);
            }
            _log.Debug("Config read");
        }


        public DirectoryInfo FlickrUploadPath
        {
            get { return new DirectoryInfo(_configuration["FlickrUploadPath"]); }
        }
    }
}