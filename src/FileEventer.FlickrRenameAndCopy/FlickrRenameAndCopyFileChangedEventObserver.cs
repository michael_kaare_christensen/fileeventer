﻿using System;
using System.IO;
using Michaelkc.FileEventer;
using Serilog;

namespace FileEventer.FlickrRenameAndCopy
{
    public class FlickrRenameAndCopyFileChangedEventObserver : IObserver<FileChangedEvent>
    {
        private readonly ILogger _log = Log.Logger.ForContext<FlickrRenameAndCopyConfiguration>();
        private readonly ImageRenamer _imageRenamer;

        public FlickrRenameAndCopyFileChangedEventObserver(ImageRenamer imageRenamer)
        {
            _imageRenamer = imageRenamer;
        }
        public void OnNext(FileChangedEvent value)
        {
            _log.Information(string.Format("Processing {0}", value));
            if (string.Equals(
                Path.GetExtension(value.FullPath), 
                ".jpg", 
                StringComparison.OrdinalIgnoreCase))
            {
                _imageRenamer.Process(new FileInfo(value.FullPath));
            }
        }

        public void OnError(Exception error)
        {
        }

        public void OnCompleted()
        {
        }
    }
}