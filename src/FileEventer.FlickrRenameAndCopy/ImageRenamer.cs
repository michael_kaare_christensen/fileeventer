﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Serilog;
using TagLib;
using File = TagLib.File;

namespace FileEventer.FlickrRenameAndCopy
{
    public class ImageRenamer
    {
        private readonly ILogger _log = Log.Logger.ForContext<ImageRenamer>();
        private readonly FlickrRenameAndCopyConfiguration _configuration;
        private FileInfo _originalFile;

        public ImageRenamer(FlickrRenameAndCopyConfiguration configuration)
        {
            _configuration = configuration;
        }

        private bool Validate()
        {
            if (!_originalFile.Exists || 
                _originalFile.Extension.ToLowerInvariant() != ".jpg" || 
                !IsValidImageFile())
            {
                _log.Debug("Not a valid JPG file: {0}", _originalFile.FullName);
                return false;
            }
            return true;
        }

        private bool IsValidImageFile()
        {
            return GetDateTimeOriginalFromExif() != DateTime.MinValue;
        }


        private DateTime GetDateTimeOriginalFromExif()
        {
            File file;
            try
            {
                file = File.Create(_originalFile.FullName);
            }
            catch (UnsupportedFormatException)
            {
                return DateTime.MinValue;
            }
            catch (CorruptFileException)
            {
                return DateTime.MinValue;
            }
            var image = file as TagLib.Image.File;
            if (image == null)
            {
                return DateTime.MinValue;
            }
            var dateTime = image.ImageTag.DateTime;
            return dateTime ?? DateTime.MinValue;
        }

        private static string HashFile(string filename)
        {
            byte[] hashedData = null;

            // Create the stream
            using (FileStream fs = System.IO.File.Open(filename, FileMode.Open, FileAccess.Read))
            {
                // MD5 is broken, but we are using it as a CRC style "detect same files", not as a crypto hash
                using (var hashAlgorithm = new MD5CryptoServiceProvider())
                {
                    hashedData = hashAlgorithm.ComputeHash(fs);
                }
            }
            string hashString = hashedData.Aggregate("", (current, t) => current + Convert.ToString(t, 16).PadLeft(2, '0'));

            return hashString.PadLeft(32, '0');
        }

        private void RenameOriginal(DirectoryInfo directory)
        {
            var created = GetDateTimeOriginalFromExif();
            var id = HashFile(_originalFile.FullName);
            var newName = created.ToString("s").Replace(':', '.') + "_" + id + ".jpg";
            var newOriginalFile = new FileInfo(Path.Combine(directory.FullName, newName));
            if (newOriginalFile.Exists)
            {
                _log.Debug("Skipping existing file {0}", newOriginalFile);
                return;
            }
            _log.Information(string.Format("Moving {0} to {1}", _originalFile, newOriginalFile));
            System.IO.File.Move(_originalFile.FullName, newOriginalFile.FullName);
            _originalFile = newOriginalFile;
        }


        public void Process(FileInfo fileInfo)
        {
            _originalFile = fileInfo;
            if (!Validate())
            {
                return;
            }
            RenameOriginal(_configuration.FlickrUploadPath);
        }
    }
}